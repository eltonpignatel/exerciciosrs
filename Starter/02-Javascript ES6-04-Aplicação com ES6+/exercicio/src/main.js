import api from './api';

class App {
    constructor() {
        this.repositories = [];
        this.formEl = document.getElementById('repo-form');
        this.listEl = document.getElementById('repos-ul');
        this.inputEl = document.querySelector('input');

        this.registerHandlers();
    }
    
    registerHandlers() {
        this.formEl.onsubmit = event => this.addRepository(event);
    }

    setLoading(loading = true){
        if (loading) {
            let loadingEl = document.createElement('span');
            loadingEl.appendChild(document.createTextNode('Carregando...'));
            loadingEl.setAttribute('id','loading');
            this.formEl.appendChild(loadingEl);
        } else {
            document.getElementById('loading').remove();
        }
    }

    async addRepository(event) {
        event.preventDefault();

        const repoInput = this.inputEl.value;

        if (repoInput.length === 0)
            return;

        this.setLoading();

        try {
            const response = await api.get(`repos/${repoInput}`);
        
            const {name, description, owner: {avatar_url}, html_url} = response.data;
    
            this.repositories.push({
                name,
                description,
                avatar_url,
                html_url
            });
            
            this.inputEl.value = '';

            this.render();

        } catch(err) {
            alert('Erro ao buscar repositório');
        }

        this.setLoading(false);
        
    }

    render() {
        this.listEl.innerHTML = '';

        this.repositories.forEach( repo => {

            let elImg = document.createElement('img');
            elImg.setAttribute('src',repo.avatar_url)
            
            let elStrong = document.createElement('strong');
            elStrong.appendChild(document.createTextNode(repo.name));
            
            let elp = document.createElement('p');
            elp.appendChild(document.createTextNode(repo.description==null ? '' : repo.description));
            

            let elLink = document.createElement('a');
            elLink.setAttribute('href', repo.html_url);
            elLink.setAttribute('target', 'blank');
            elLink.appendChild(document.createTextNode('Acessar'));

            let elLi = document.createElement('li');
            elLi.appendChild(elImg)
            elLi.appendChild(elStrong);
            elLi.appendChild(elp);
            elLi.appendChild(elLink);

            this.listEl.appendChild(elLi);
        });

    }

}

new App();