const delay = () => new Promise(resolve => setTimeout(resolve, 1000));

//Exercicio 1
const exercicio1 = async function (resolve){
  await delay()
  console.log("1seg");
  await delay()
  console.log("2seg");
  await delay()
  console.log("3seg");
}

exercicio1();

//Exercicio 2

import axios from 'axios';

async function getUsersFromGithub(user) {
  try {
    const response = await axios.get(`https://api.github.com/users/${user}`);
    console.log(response);
  } catch(erro) {
    console.warn(erro);
  }
}

getUsersFromGithub('eltonpignatel');
getUsersFromGithub('eltonpignatel12345');

//Exercicio 3
class Github {
  static async getRepositories(repo) {
    const response = await axios.get(`https://api.github.com/users/${repo}/repos`);
    console.log(response);
  } catch(err) {
    console.warn('Erro ao buscar repositórios - ' + err);
  }
    
}

Github.getRepositories('pljson');
Github.getRepositories('rocketseat/dslkvmskv');

const buscaUsuario = async usuario => {
  try{
    const response = await axios.get(`https://api.github.com/users/${usuario}`);
    console.log(response.data);
  } catch (erro) {
    console.log('Usuario não existe -' + erro);
  }
  
}

buscaUsuario('eltonpignatel');